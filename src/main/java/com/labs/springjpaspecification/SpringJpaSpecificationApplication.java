package com.labs.springjpaspecification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaSpecificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaSpecificationApplication.class, args);
	}

}
